/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.orderproject;

/**
 *
 * @author ทักช์ติโชค
 */
public interface OnBuyProductListener {
    public  void buy(Product product, int amount);
    
}
