/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.takitclhoke.orderproject;



/**
 *
 * @author acer
 */
public class OrderItem {

    Product product;
    int amonut;

    public OrderItem(Product product, int amonut) {
        this.product = product;
        this.amonut = amonut;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getAmonut() {
        return amonut;
    }

    public void setAmonut(int amonut) {
        this.amonut = amonut;
    }

}