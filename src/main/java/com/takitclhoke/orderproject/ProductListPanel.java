/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.orderproject;

import java.awt.GridLayout;
import java.util.ArrayList;

/**
 *
 * @author ทักช์ติโชค
 */
public class ProductListPanel extends javax.swing.JPanel implements OnBuyProductListener {
    
    private ArrayList<OnBuyProductListener> subcrlberList = new ArrayList<>();
    private final ArrayList<Product> productlist;
    private OrderPanel orderPanel;

    /**
     * Creates new form ProductListPanel
     */
    public ProductListPanel() {
        initComponents();
        
        productlist = Product.getMockProductList();
        
        for (Product product : productlist) {
            ProductPanel panel = new ProductPanel(product);
            prdouctPanel.add(panel);
            panel.addOnbuyListener(this);
            
        }
        
        int num = prdouctPanel.getComponentCount();
        int col = 3;
        prdouctPanel.setLayout(new GridLayout(num / col + (num % col > 0 ? 1 : 0), col));
    }
    
    public void addOnBuyListenner(OnBuyProductListener subcrlber) {
        subcrlberList.add(subcrlber);
    }
    
    public void setOrderPanel(OrderPanel orderPanel) {
        this.orderPanel = orderPanel;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        prdouctPanel = new javax.swing.JPanel();

        javax.swing.GroupLayout prdouctPanelLayout = new javax.swing.GroupLayout(prdouctPanel);
        prdouctPanel.setLayout(prdouctPanelLayout);
        prdouctPanelLayout.setHorizontalGroup(
            prdouctPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 525, Short.MAX_VALUE)
        );
        prdouctPanelLayout.setVerticalGroup(
            prdouctPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 381, Short.MAX_VALUE)
        );

        jScrollPane1.setViewportView(prdouctPanel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 527, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 383, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel prdouctPanel;
    // End of variables declaration//GEN-END:variables

    @Override
    public void buy(Product product, int amount) {
//        orderPanel.buy(product, amount);
        for (OnBuyProductListener s : subcrlberList) {
            s.buy(product, amount);
        }
    }
}
